import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class configScripts {
	public static int copyCookies(String confName) {
	    try {
	    	/**
	    	 * Return:
	    	 	* 1 - OK
	    	 	* 2 - Nezn�m� chyba
	    	 	* 3 - Chyba p�i vytv��en� slo�ek
	    	 	* 4 - Chyba pr�i kop�rov�n�  
	    	 */
	  
	    	// Nastaven� source path
		    	String srcPath = (System.getenv("APPDATA"));
		    	String srcPathFinal = (srcPath.replace("Roaming", "Local\\Google\\Chrome\\User Data\\Default\\Cookies"));
		    	File src = new File(srcPathFinal);
	    	// Nastaven� taget path
		    	String targetPath = System.getProperty("user.dir");
		    	String tagetPathFinal = targetPath+"\\chrome_profiles\\"+confName+"\\Default\\Cookies";
		    	File target = new File(tagetPathFinal);
		    // Nastaven� clear path (slo�ka, kter� bude promaz�na)
		    	String clearPath = targetPath+"\\chrome_profiles\\"+confName;
		    	File clearPathFile = new File(clearPath);
			// Vytvo�en� chrome_profiles slo�ky
		    	File chromeDir = new File(targetPath+"\\chrome_profiles\\");
		    	if (!chromeDir.exists()) {
		    	    try{
		    	    	chromeDir.mkdir();
		    	    } 
		    	    catch(SecurityException se){
		    	        return 3;
		    	    }        
		    	}
			 //Vytvo�en� target slo�ky se jm�nem ��tu
		    	File theDir = new File(clearPath);
		    	if (!theDir.exists()) {
		    	    try{
		    	        theDir.mkdir();
		    	    } 
		    	    catch(SecurityException se){
		    	        return 3;
		    	    }        
		    	}
	    	//Promaz�n� target slo�ky
		    	FileUtils.cleanDirectory(clearPathFile);
		    //Vytvo�en� target slo�ky pro samotn� cookies
		    	File cookieDir = new File(clearPath+"\\Default");
		    	if (!cookieDir.exists()) {
		    	    try{
		    	    	cookieDir.mkdir();
		    	    } 
		    	    catch(SecurityException se){
		    	        return 3;
		    	    }        
		    	}
	    	//Samotn� kop�rov�n�
	    	    try{
	    	    	Files.copy(src.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);
	    	    } 
	    	    catch(SecurityException se){
	    	        return 4;
	    	    }        
	    	
		    
	    	
	    } catch (Exception exc) {
	      exc.printStackTrace();
	      return 2;
	    }

	    return 1;
	}
	public static int testCookies(String confName, String igIp, String igPort, String igType) {
	    try {
	    	/**
	    	 * Return:
	    	 	* 1 - OK
	    	 	* 2 - Nezn�m� chyba
	    	 	* 3 - Chyba p�i vytv��en� slo�ek
	    	 	* 4 - Chyba pr�i kop�rov�n�
	    	 */
	  
	    	//Nastaven� path do pracovn� slo�ky
		    	String targetPath = System.getProperty("user.dir");
		    //Nastaven� path pro dan� ��et
		    	String finalPath = targetPath+"\\chrome_profiles\\"+confName;
			//Zji�t�n�, jestli existuje slo�ka s chrome profilem pro tento acc
		    	File chromeDir = new File(targetPath+"\\chrome_profiles\\");
		    	if (!chromeDir.exists()) {
		    	    try{
		    	    	chromeDir.mkdir();
		    	    } 
		    	    catch(SecurityException se){
		    	        return 3;
		    	    }        
		    	}
			 //Vytvo�en� target slo�ky se jm�nem ��tu
		    	File theDir = new File(finalPath);
		    	if (!theDir.exists()) {
		    	    try{
		    	        theDir.mkdir();
		    	    } 
		    	    catch(SecurityException se){
		    	        return 3;
		    	    }        
		    	}
		    //Spu�t�n� chromedriveru pro tuto slo�ku
			     ChromeOptions chromeOptions = new ChromeOptions();
			     //Nastaven� Chrome options
				      chromeOptions.addArguments("user-data-dir="+finalPath);
				      chromeOptions.setBinary("C:/Users/Maxim/AppData/Local/Google/Chrome SxS/Application/chrome.exe");
				 //Nastaven� proxy IP
				      //Pokud je type socks
				      System.out.println(igType);
				      	if (igType.equals("socks")) {
				      		chromeOptions.addArguments("--proxy-server=socks5://" + igIp + ":" + igPort);
	System.out.println("Debug [configSelenium]: Proxy nastavena na typ Socks5, na��t�n� ip: "+igIp+" s portem "+igPort);
				      	}
			    //Start Chrome
				      	WebDriver Driver = new ChromeDriver(chromeOptions);
	System.out.println("Debug [configSelenium]: Otev�r�n� cookies profilu "+finalPath); 
				//* Samotn� ov��en� cookies
			      Driver.navigate().to("https://www.instagram.com/"+confName);  
	    	
		    
	    	
	    } catch (Exception exc) {
	      exc.printStackTrace();
	      return 2;
	    }

	    return 1;
	}
}
