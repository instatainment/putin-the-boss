import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import com.mysql.jdbc.PreparedStatement;

/**
 * TODO
 *  => Insert jobs
 *  	-> Promyslet strukturu SQL
 * @author Maxim
 *
 */
public class dbScripts {
	public static int jobsAssign(String ConnDB_name, String ConnDB_pass, String ConnDB_link, String ConnDB_dbName, List<String> accList, int sessionID) {
	    try {
	    	/**
	    	 * Return:
	    	 	* 1 - OK
	    	 	* 2 - Nezn�m� chyba v p�i�azov�n� basic jobs (prvn� ��st scriptu)
	    	 	* 3 - Nezn�m� chyba v p�i�azov�n� inkSourceScanning (druh� ��st scriptu)
	    	 	* 4 - Nelze p�ipojit k SQL
	    	 */
	    	// Nav�z�n� p�ipojen�
	    		Connection myConn = DriverManager.getConnection("jdbc:mysql://"+ConnDB_link+"/"+ConnDB_dbName, ConnDB_name, ConnDB_pass);
	    	// Vytvo�en� statement
	    		Statement myStmt = myConn.createStatement();
	    	
	    	// Vytvo�en� pomocn�ch list�
	    		List<String> inkAllAccs = new ArrayList<String>();
	    		inkAllAccs.addAll(accList);
	
	    	/**
	    	 * P�i�azen� z�kladn�ch jobs => Follow, Unfollow, Like, Comment
	    	 */
//System.out.println("Debug [dbSripts]: Za��tek generov�n� basic jobs");
	    	        try { 
	    	        	// Kontrola, ke kter�mu ��tu bude mo�nost pr�ci p�i�adit
				            String sql = "SELECT assigned_to FROM jobs WHERE (added_by = 'bot-autoDistribute' AND session = '"+sessionID+"')";
							ResultSet rs = myStmt.executeQuery(sql);
							// P�id�n� v�ech nalezen�ch non-duplicate ��t� do listu
								List<String> assignedFound = new ArrayList<String>();
								while(rs.next()){
									// Z�sk�n� assigned ��tu
										String assignedName = rs.getString("assigned_to");
									if (assignedName != null && assignedName != "") {
										// P�id�n� do listu
											assignedFound.add(assignedName);
									}
								} // Dokud nejsou prohled�ny v�echny non-duplicaty
						
						// Porovn�n� v�ech account� s non-dublicate listem - List accList jsou nyn� acc ready na p�id�n� jobs
							accList.removeAll(assignedFound);
						// Z�sk�n� po�tu opakov�n� (kolik bylo non-duplicatu nalezeno)
							int countFound = accList.size();
						// Prov�st v�echny opakov�n�
						for (int x = 0; x < countFound; x++) {
					    	// Z�sk�n� nalezen�ho jm�na z listu
					    		String assignedName = accList.get(x);	
					    	// Zji�t�n� speci�ln�ch nastaven� pro tento ��et
					    		Statement stmtTwo = myConn.createStatement();
        			            String sqlConf = "SELECT * FROM accounts WHERE username = '"+assignedName+"' LIMIT 	1";
        						ResultSet sqlConfSettings = stmtTwo.executeQuery(sqlConf);
        					// Extraktov�n� dat z result set -> Postupn� proch�zen�
        					String jobsBlocked = "";
        					int jobsPause = 250;
					            while(sqlConfSettings.next()){
		            				jobsPause 		= sqlConfSettings.getInt("jobs_pause");
		            				jobsBlocked		= sqlConfSettings.getString("jobs_blocked");
					            }
					            List<String> jobsBlockedList = Arrays.asList(jobsBlocked.split(","));
					    	// Zpracov�n� blokovan�ch jobs
					            List<String> jobsBlockedReal = new ArrayList<String>();
					    		if (jobsBlockedList.contains("follow")) {
					    			jobsBlockedReal.add("follow");
					    		}
					    		if (jobsBlockedList.contains("unfollow")) {
					    			jobsBlockedReal.add("unfollow");
					    		}
					    		if (jobsBlockedList.contains("comment")) {
					    			jobsBlockedReal.add("comment");
					    		}
					    		if (jobsBlockedList.contains("like")) {
					    			jobsBlockedReal.add("like");
					    		}
					    		List<String> jobsList = new ArrayList<String>();
					    		jobsList.addAll(Arrays.asList("follow", "unfollow", "comment", "like"));
					    		jobsList.removeAll(jobsBlockedReal);
					    		int genCount = jobsList.size();
					    	// Generov�n� n�hodn� job
					    		Random rand = new Random();
					    		int jobGenerated = rand.nextInt(genCount);
					    		String jobName = jobsList.get(jobGenerated);
System.out.println("Debug [dbSripts]: Generuje se n�hodn� job z v�beru: "+jobsList);				    		

					    	// Generov�n� n�hodn�ho �asu podle p�edlohy
					    	    Timestamp timestamp = new Timestamp(new Date().getTime());
					    	    Calendar cal = Calendar.getInstance();
					    	    cal.setTimeInMillis(timestamp.getTime());
					    	    // P�idat x sekund
					    	    	// Vygenerovat n�hodn� �as z intervalu
					    				int randMin = jobsPause - 20;
					    				int randMax = jobsPause + 20;
					    				int pauseGenerated = ThreadLocalRandom.current().nextInt(randMin, randMax + 1);
						    	    cal.add(Calendar.SECOND, pauseGenerated);
						    	    timestamp = new Timestamp(cal.getTime().getTime());
						    	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						    	    String modTime  = dateFormat.format(timestamp);
					    		
System.out.println("Debug [dbSripts]: Byly n�hodn� vygenerov�ny Job: "+jobName+", za "+pauseGenerated+" sekund pro "+assignedName);
		    	            // Vytvo�en� INSERT INTO statementu
			    	            String query = 	" INSERT INTO jobs (job, progress, assigned_to, assigned_time, added_by, session)" + 
			    	            				" VALUES (?, ?, ?, ?, ?, ?)";

	    	            // P��prava hodnot, kter� budou do tabulky vlo�eny
		    	            PreparedStatement preparedStmt = (PreparedStatement) myConn.prepareStatement(query);
		    	            preparedStmt.setString (1, jobName);
		    	            preparedStmt.setString (2, "assigned");
		    	            preparedStmt.setString (3, assignedName);
		    	            preparedStmt.setString (4, modTime);
		    	            preparedStmt.setString (5, "bot-autoDistribute");
		    	            preparedStmt.setInt (6, sessionID);
	    	            // Vlo�it do tabulky
	    	            	preparedStmt.execute();
						} // Prov�st v�echny opakov�n�


	    	        } catch (Exception e) {
	    	            System.err.println(e.getMessage());
	    	        	return 2;
	    	        }
	    	        
	    	/**
	    	 * P�i�azen� pravideln�ch jobs => INK scanning
	    	 */
	    	        try { 
	    	        	// Kontrola, ke kter�mu ��tu bude mo�nost pr�ci p�i�adit
				            String sql = "SELECT assigned_to FROM jobs WHERE (added_by = 'bot-autoDistribute' AND session = '"+sessionID+"' AND job = 'inkScannning')";
							ResultSet rs = myStmt.executeQuery(sql);
							// P�id�n� v�ech nalezen�ch non-duplicate ��t� do listu
								List<String> inkFound = new ArrayList<String>();
								while(rs.next()){
									// Z�sk�n� assigned ��tu
										String assignedName = rs.getString("assigned_to");
									if (assignedName != null && assignedName != "") {
										// P�id�n� do listu
										inkFound.add(assignedName);
									}
								} // Dokud nejsou prohled�ny v�echny non-duplicaty
						
						// Porovn�n� v�ech account� s non-dublicate listem - List accList jsou nyn� acc ready na p�id�n� jobs
							inkAllAccs.removeAll(inkFound);
						// Z�sk�n� po�tu opakov�n� (kolik bylo non-duplicatu nalezeno)
							int countFound = inkAllAccs.size();
						// Prov�st v�echny opakov�n�
						for (int x = 0; x < countFound; x++) {
					    	// Z�sk�n� nalezen�ho jm�na z listu
					    		String assignedName = inkAllAccs.get(x);	
        					// Extraktov�n� dat z result set -> Postupn� proch�zen�
        					int jobsPause = 100;			    		

					    	// Generov�n� n�hodn�ho �asu podle p�edlohy
					    	    Timestamp timestamp = new Timestamp(new Date().getTime());
					    	    Calendar cal = Calendar.getInstance();
					    	    cal.setTimeInMillis(timestamp.getTime());
					    	    // P�idat x sekund
					    	    	// Vygenerovat n�hodn� �as z intervalu
					    				int randMin = jobsPause - 20;
					    				int randMax = jobsPause + 20;
					    				int pauseGenerated = ThreadLocalRandom.current().nextInt(randMin, randMax + 1);
						    	    cal.add(Calendar.SECOND, pauseGenerated);
						    	    timestamp = new Timestamp(cal.getTime().getTime());
						    	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						    	    String modTime  = dateFormat.format(timestamp);
					    		
System.out.println("Debug [dbSripts]: Byl p�i�azen INK scanning za "+pauseGenerated+" sekund pro "+assignedName);
		    	            // Vytvo�en� INSERT INTO statementu
			    	            String query = 	" INSERT INTO jobs (job, progress, assigned_to, assigned_time, added_by, session)" + 
			    	            				" VALUES (?, ?, ?, ?, ?, ?)";

	    	            // P��prava hodnot, kter� budou do tabulky vlo�eny
		    	            PreparedStatement preparedStmt = (PreparedStatement) myConn.prepareStatement(query);
		    	            preparedStmt.setString (1, "inkScannning");
		    	            preparedStmt.setString (2, "assigned");
		    	            preparedStmt.setString (3, assignedName);
		    	            preparedStmt.setString (4, modTime);
		    	            preparedStmt.setString (5, "bot-autoDistribute");
		    	            preparedStmt.setInt (6, sessionID);
	    	            // Vlo�it do tabulky
	    	            	preparedStmt.execute();
						} // Prov�st v�echny opakov�n�


	    	        } catch (Exception e) {
	    	            System.err.println(e.getMessage());
	    	        	return 3;
	    	        }
	    		
	    		
	    		
	    //Ukon�en� SQL (p�ipojen�)
	    	myConn.close();	
	    }
	    catch (Exception exc) {
	    	return 4;
	    }
	    //Pokud v�e prob�hne v po��dku
	    	return 1;
	    
	}
	
	public static int scriptReset(String ConnDB_name, String ConnDB_pass, String ConnDB_link, String ConnDB_dbName, List<String> accList, int session) {
	    try {
	    	/**
	    	 * Return:
	    	 	* 1 - OK
	    	 	* 2 - Nezn�m� chyba
	    	 	* 3 - 
	    	 	* 4 - Nelze p�ipojit k SQL
	    	 */
	    	// Nav�z�n� p�ipojen�
	    		Connection myConn = DriverManager.getConnection("jdbc:mysql://"+ConnDB_link+"/"+ConnDB_dbName, ConnDB_name, ConnDB_pass);
	
	    	/*
	    	 * Smaz�n� v�ech jobs pro jm�na z accListu
	    	 */
	    	        try { 
        				// SQL
        					String query = 	" DELETE FROM jobs WHERE (session = '"+session+"' AND added_by = 'bot-autoDistribute')";
        				PreparedStatement preparedStmt = (PreparedStatement) myConn.prepareStatement(query);
    	            	preparedStmt.execute();
	    	        } catch (Exception e) {
	    	            System.err.println(e.getMessage());
	    	        	return 2;
	    	        } 
	   			
	    //Ukon�en� SQL (p�ipojen�)
	    	myConn.close();	
	    }
	    catch (Exception exc) {
	    	return 4;
	    }
	    //Pokud v�e prob�hne v po��dku
    	return 1;
	}
}
