import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mysql.jdbc.PreparedStatement;

public class seleniumScripts {
	//*************************************************************************************************************************************************************************************************************************************//	
	public static int checkCookies(String ConnDB_name, String ConnDB_pass, String ConnDB_link, String ConnDB_dbName, String igName, String igIp, String igPort, String igType) {
	    try {
	    	/**
	    	 * Return:
	    	 	* 1 - OK
	    	 	* 2 - Nezn�m� chyba
	    	 	* 3 - Chyba p�i hled�n� elementu (selenium)
	    	 	* 4 - Nelze p�ipojit k SQL
	    	 */
		     ChromeOptions chromeOptions = new ChromeOptions();
		     // Z�skat aktu�ln� slo�ku
		      	String systemPath = System.getProperty("user.dir");
		     // Nastaven� Chrome options
			      chromeOptions.addArguments("user-data-dir="+systemPath+"//chrome_profiles//"+igName);
			      chromeOptions.addArguments("--headless");
			      chromeOptions.setBinary("C:/Users/Maxim/AppData/Local/Google/Chrome SxS/Application/chrome.exe");
			 // Nastaven� proxy IP
			      //Pokud je type socks
			      	if (igType.equals("socks")) {
			      		chromeOptions.addArguments("--proxy-server=socks5://" + igIp + ":" + igPort);
System.out.println("Debug [seleniumScripts]: Proxy nastavena na typ Socks5, na��t�n� ip: "+igIp+" s portem "+igPort);
			      	}
		    // Start Chrome
			      	WebDriver Driver = new ChromeDriver(chromeOptions);
System.out.println("Debug [seleniumScripts]: Otev�r�n� cookies profilu "+systemPath+"//chrome_profiles//"+igName); 
			//* Samotn� ov��en� cookies
		      Driver.navigate().to("https://www.instagram.com/"+igName);
		      Thread.sleep(4000);
		      try {
			      int login = Driver.findElements(By.xpath("//button[@class='_q8y0e coreSpriteMobileNavSettings _8scx2']")).size();
			      if (login == 0) {
			    	  Driver.quit();
			    	  return 3;
			      } else {
			    	  Driver.quit();
			    	  return 1;
			      }
			  
		      } catch (Exception exc) {
			    	Driver.quit();
			    	return 3;
		      }	    
	    } catch (Exception exc) {
	      exc.printStackTrace();
	      	return 4;
	    }
	    
	}
	
//*************************************************************************************************************************************************************************************************************************************//
	
	public static int inkScanning(String ConnDB_name, String ConnDB_pass, String ConnDB_link, String ConnDB_dbName, String igName) {
	    try {
	    	/**
	    	 * Return:
	    	 	* 1 - OK
	    	 	* 2 - Nezn�m� chyba
	    	 	* 3 - Nebyla na�tena str�nka nebo cookies
	    	 	* 4 - Nelze p�ipojit k SQL
	    	 */
	    	System.out.println("Pro ��et"+igName);
	    	// Z�sk�n� informace o cookies p�idru�en�ch k tomuto ��etu z DB
				Connection myConn = DriverManager.getConnection("jdbc:mysql://"+ConnDB_link+"/"+ConnDB_dbName, ConnDB_name, ConnDB_pass);
				Statement checkState = myConn.createStatement();
	            String sqlRun = "SELECT inkScanning_id FROM accounts WHERE username = '"+igName+"' LIMIT 1";
	            String scanID = "x";
				ResultSet sqlRes = checkState.executeQuery(sqlRun);
		            //Extraktov�n� dat z result set -> Postupn� proch�zen�
			            while(sqlRes.next()){
			            	scanID = sqlRes.getString("inkScanning_id");
			            }  

		     ChromeOptions chromeOptions = new ChromeOptions();
		     //Z�skat aktu�ln� slo�ku
		      	String systemPath = System.getProperty("user.dir");
		     //Nastaven� Chrome options
			      chromeOptions.addArguments("user-data-dir="+systemPath+"//chrome_profiles//ink"+scanID);
			      //chromeOptions.addArguments("--headless");
			      chromeOptions.setBinary("C:/Users/Maxim/AppData/Local/Google/Chrome SxS/Application/chrome.exe");
			 //Nastaven� proxy IP
			      //Pokud je type socks

		    //Start Chrome
			      	WebDriver Driver = new ChromeDriver(chromeOptions);
System.out.println("Debug [seleniumScripts]: Otev�r�n� inkScanningu profilu "+systemPath+"//chrome_profiles//"+igName); 
			//* Samotn� ov��en� cookies
		      Driver.navigate().to("https://ink361.com/app/circles");
		      Thread.sleep(4000);
		      try {
			      int cookiesSuccess = Driver.findElements(By.xpath("//div[@class='panel']")).size();
			      if (cookiesSuccess == 0) {
			    	  // Pokud jsou nastaveny �patn� cookies
			    	  Driver.quit();
			            // Smaz�n� z�znamu z SQL
						
							  // Vytov�en� statementu
								PreparedStatement deleteState = (PreparedStatement) myConn.prepareStatement(
									"DELETE FROM jobs WHERE (job = ? AND assigned_to = ?)");
							
							  // Nastaven� parametr�
								deleteState.setString(1,"comment");
								deleteState.setString(2,igName);
							
							  // Zavol�n� funkce updatu a ukon�en� p�ipojen�
								deleteState.executeUpdate();
								deleteState.close();
   
			    	  return 3;
			      } else {
			    	  Driver.quit();
			    	  return 1;
			      }
			  
		      } catch (Exception exc) {
			    	Driver.quit();
			    	return 3;
		      }	    
	    } catch (Exception exc) {
	      exc.printStackTrace();
	      	return 4;
	    }
	    
	}
	
}
