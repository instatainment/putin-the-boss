import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.ScrollPane;
/**
 * ToDo list
 * 	PROGRESS
 * ->
 * 
 * 	PREPARED
 *  -> Maximalizovat prohl�e�, p�idat headless
 *  -> P�idat job scanning - vymyslet syst�m a druhy ostatn�ch jobs
 *  
 *  FUTURE
 *  -> P�idat podporu pro minim�ln� 20 ��t�
 *  	- Upravit GUI (pop�em��let nad scrollem
 *  	- Assign dan�ch ��t� pro session (p�ed kliknut�m na start tla��tko vybrat session) - pou��vat aktu�ln� session ID
 *  -> N�kter� jobs p�i�azovat s odezvou 6 hodin (p�es SQL nacpat job za 6 hodin, nebude se vytv��et automaticky nov�)
 *  -> Config Scripts - kop�rovat cookies ne z appdata ale tak, aby se dalo kombinovat s testem .. (domyslet)
 * 		- P�idat error handeling config scrip�
 * 		- P�idat Debug koment��e ke v�em akc�m
 * 
 * @author Maxim
 *
 */
public class mainClass{

	private JFrame frame;
	private JLabel textStarting;
	private JLabel bottomCopyright;
	private JLabel lblSessionSummon;
	private JLabel guiBackground;
	private JLabel cmdLineBackground;
	private JLabel bottomTextQuote;
	private JLabel cmdAccName1;
	private JLabel cmdAccName2;
	private JLabel cmdAccName3;
	private JLabel cmdAccName4;
	private JLabel cmdAccName5;
	private JLabel cmdAccName6;
	private JLabel configBackground;
	private JButton confButtonAcc1;
	private JButton confButtonAcc2;
	private JButton confButtonAcc3;
	private JButton confButtonAcc4;
	private JButton confButtonAcc5;
	private JButton confButtonAcc6;
	private JButton confButtonAcc7;
	private JButton confButtonAcc8;
	private JButton confButtonAcc9;
	private JButton confButtonAcc10;

	/**
	 * Spu�t�n� aplikace
	 */
	public static void main(String[] args) throws InterruptedException{	
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					mainClass window = new mainClass();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Sestaven� aplikace
	 * @throws SQLException 
	 */
	public mainClass() throws SQLException {
		initialize();
	}


	/**
	 * Inicializace contentu
	 * @throws SQLException 
	 * @throws InterruptedException 
	 */

	private void initialize() throws SQLException {
		/**
		 * Nastaven� z�kladn�ch prom�nn�ch
		 */
			// P�ipojen� SQL
				String ConnDB_name = "testing";
				String ConnDB_pass = "Test123";
				String ConnDB_link = "sql.endora.cz:3314";
				String ConnDB_dbName = "dbbot";
			// Session ID TODO do�asn�
				int sessionID = 1234567899;

		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 850, 525);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setResizable(false);
		
		JButton buttonMain = new JButton("Spustit");
		buttonMain.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		buttonMain.setBackground(Color.ORANGE);
		
		JButton buttonEnd = new JButton("Ukon\u010Dit");
		buttonEnd.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		buttonEnd.setBackground(Color.ORANGE);
		buttonEnd.setBounds(343, 435, 164, 37);
		buttonEnd.setOpaque(true);
		buttonEnd.setVisible(false);
		
		
		confButtonAcc1 = new JButton("Button");
		confButtonAcc1.setBounds(45, 63, 171, 23);
		confButtonAcc1.setVisible(false);
		frame.getContentPane().add(confButtonAcc1);
		
		confButtonAcc2 = new JButton("Button");
		confButtonAcc2.setBounds(45, 93, 171, 23);
		confButtonAcc2.setVisible(false);
		frame.getContentPane().add(confButtonAcc2);
		
		confButtonAcc3 = new JButton("Button");
		confButtonAcc3.setBounds(45, 127, 171, 23);
		confButtonAcc3.setVisible(false);
		frame.getContentPane().add(confButtonAcc3);
		
		confButtonAcc4 = new JButton("Button");
		confButtonAcc4.setBounds(45, 162, 171, 23);
		confButtonAcc4.setVisible(false);
		frame.getContentPane().add(confButtonAcc4);
		
		confButtonAcc5 = new JButton("Button");
		confButtonAcc5.setBounds(45, 199, 171, 23);
		confButtonAcc5.setVisible(false);
		frame.getContentPane().add(confButtonAcc5);
		
		confButtonAcc6 = new JButton("Button");
		confButtonAcc6.setBounds(355, 63, 171, 23);
		confButtonAcc6.setVisible(false);
		frame.getContentPane().add(confButtonAcc6);
		
		confButtonAcc7 = new JButton("Button");
		confButtonAcc7.setBounds(355, 93, 171, 23);
		confButtonAcc7.setVisible(false);
		frame.getContentPane().add(confButtonAcc7);
		
		confButtonAcc8 = new JButton("Button");
		confButtonAcc8.setBounds(355, 127, 171, 23);
		confButtonAcc8.setVisible(false);
		frame.getContentPane().add(confButtonAcc8);
		
		confButtonAcc9 = new JButton("Button");
		confButtonAcc9.setBounds(355, 162, 171, 23);
		confButtonAcc9.setVisible(false);
		frame.getContentPane().add(confButtonAcc9);
		
		confButtonAcc10 = new JButton("Button");
		confButtonAcc10.setBounds(355, 199, 171, 23);
		confButtonAcc10.setVisible(false);
		frame.getContentPane().add(confButtonAcc10);
		
		JButton confButtonEdit1 = new JButton("Edit / Test");
		confButtonEdit1.setBounds(226, 63, 91, 23);
		confButtonEdit1.setVisible(false);
		frame.getContentPane().add(confButtonEdit1);
		
		JButton confButtonEdit2 = new JButton("Edit / Test");
		confButtonEdit2.setBounds(226, 93, 91, 23);
		confButtonEdit2.setVisible(false);
		frame.getContentPane().add(confButtonEdit2);
		
		JButton confButtonEdit3 = new JButton("Edit / Test");
		confButtonEdit3.setBounds(226, 127, 91, 23);
		confButtonEdit3.setVisible(false);
		frame.getContentPane().add(confButtonEdit3);
		
		JButton confButtonEdit4 = new JButton("Edit / Test");
		confButtonEdit4.setBounds(226, 162, 91, 23);
		confButtonEdit4.setVisible(false);
		frame.getContentPane().add(confButtonEdit4);
		
		JButton confButtonEdit5 = new JButton("Edit / Test");
		confButtonEdit5.setBounds(226, 199, 91, 23);
		confButtonEdit5.setVisible(false);
		frame.getContentPane().add(confButtonEdit5);
		
		JButton confButtonEdit6 = new JButton("Edit / Test");
		confButtonEdit6.setBounds(536, 63, 91, 23);
		confButtonEdit6.setVisible(false);
		frame.getContentPane().add(confButtonEdit6);
		
		JButton confButtonEdit7 = new JButton("Edit / Test");
		confButtonEdit7.setBounds(536, 93, 91, 23);
		confButtonEdit7.setVisible(false);
		frame.getContentPane().add(confButtonEdit7);
		
		JButton confButtonEdit8 = new JButton("Edit / Test");
		confButtonEdit8.setBounds(536, 127, 91, 23);
		confButtonEdit8.setVisible(false);
		frame.getContentPane().add(confButtonEdit8);
		
		JButton confButtonEdit9 = new JButton("Edit / Test");
		confButtonEdit9.setBounds(536, 162, 91, 23);
		confButtonEdit9.setVisible(false);
		frame.getContentPane().add(confButtonEdit9);
		
		JButton confButtonEdit10 = new JButton("Edit / Test");
		confButtonEdit10.setBounds(536, 199, 91, 23);
		confButtonEdit10.setVisible(false);
		frame.getContentPane().add(confButtonEdit10);
		
		JButton confLoadNext = new JButton("Next");
		confLoadNext.setFont(new Font("Tahoma", Font.PLAIN, 11));
		confLoadNext.setBounds(732, 63, 63, 23);
		confLoadNext.setVisible(false);
		frame.getContentPane().add(confLoadNext);
		
		JButton confButtonInk1 = new JButton("1");
		confButtonInk1.setBounds(665, 93, 39, 23);
		confButtonInk1.setVisible(false);
		frame.getContentPane().add(confButtonInk1);
		
		JButton confButtonInk2 = new JButton("2");
		confButtonInk2.setBounds(665, 118, 39, 23);
		confButtonInk2.setVisible(false);
		frame.getContentPane().add(confButtonInk2);
		
		JButton confButtonInk3 = new JButton("3");
		confButtonInk3.setBounds(665, 143, 39, 23);
		confButtonInk3.setVisible(false);
		frame.getContentPane().add(confButtonInk3);
		
		JButton confButtonInk4 = new JButton("4");
		confButtonInk4.setBounds(665, 168, 39, 23);
		confButtonInk4.setVisible(false);
		frame.getContentPane().add(confButtonInk4);
		
		JButton confButtonInk5 = new JButton("5");
		confButtonInk5.setBounds(665, 193, 39, 23);
		confButtonInk5.setVisible(false);
		frame.getContentPane().add(confButtonInk5);
		
		

		
		JButton confLoadPrev = new JButton("Prev");
		confLoadPrev.setFont(new Font("Tahoma", Font.PLAIN, 11));
		confLoadPrev.setBounds(678, 63, 63, 23);
		confLoadPrev.setVisible(false);
		frame.getContentPane().add(confLoadPrev);
		
		JLabel confDividingLine1 = new JLabel("");
		confDividingLine1.setBackground(new Color(255, 127, 80));
		confDividingLine1.setBounds(327, 63, 18, 159);
		confDividingLine1.setOpaque(true);
		confDividingLine1.setVisible(false);
		frame.getContentPane().add(confDividingLine1);
		
		JLabel confDividingLine2 = new JLabel("");
		confDividingLine2.setOpaque(true);
		confDividingLine2.setBackground(new Color(255, 127, 80));
		confDividingLine2.setBounds(637, 63, 18, 159);
		confDividingLine2.setVisible(false);
		frame.getContentPane().add(confDividingLine2);
		
		JLabel lblNewJgoodiesTitle = DefaultComponentFactory.getInstance().createTitle("Russian Theme Edition");
		lblNewJgoodiesTitle.setBounds(610, 483, 131, 14);
		frame.getContentPane().add(lblNewJgoodiesTitle);
		frame.getContentPane().add(buttonEnd);
		buttonMain.setBounds(343, 435, 164, 37);
		frame.getContentPane().add(buttonMain);
		
		JLabel topMenuBackground = new JLabel("");
		topMenuBackground.setBackground(Color.ORANGE);
		topMenuBackground.setBounds(0, 0, 846, 27);
		topMenuBackground.setOpaque(true);
		
		configBackground = new JLabel("");
		configBackground.setBackground(Color.ORANGE);
		configBackground.setFont(new Font("Tahoma", Font.PLAIN, 16));
		configBackground.setBounds(45, 63, 750, 159);
		configBackground.setVisible(false);
		configBackground.setOpaque(true);
		frame.getContentPane().add(configBackground);
		
		cmdAccName1 = new JLabel("");
		cmdAccName1.setBounds(61, 77, 110, 14);
		frame.getContentPane().add(cmdAccName1);
		
		cmdAccName2 = new JLabel("");
		cmdAccName2.setBounds(61, 102, 110, 14);
		frame.getContentPane().add(cmdAccName2);
		
		cmdAccName3 = new JLabel("");
		cmdAccName3.setBounds(61, 127, 110, 14);
		frame.getContentPane().add(cmdAccName3);
		
		cmdAccName4 = new JLabel("");
		cmdAccName4.setBounds(61, 152, 110, 14);
		frame.getContentPane().add(cmdAccName4);
		
		cmdAccName5 = new JLabel("");
		cmdAccName5.setBounds(61, 177, 110, 14);
		frame.getContentPane().add(cmdAccName5);
		
		cmdAccName6 = new JLabel("");
		cmdAccName6.setBounds(61, 202, 110, 14);
		frame.getContentPane().add(cmdAccName6);
		
		JLabel cmdAccJob1 = new JLabel("");
		cmdAccJob1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		cmdAccJob1.setBackground(Color.ORANGE);
		cmdAccJob1.setBounds(160, 77, 120, 14);
		frame.getContentPane().add(cmdAccJob1);
		
		JLabel cmdAccJob2 = new JLabel("");
		cmdAccJob2.setFont(new Font("Tahoma", Font.PLAIN, 10));
		cmdAccJob2.setBackground(Color.ORANGE);
		cmdAccJob2.setBounds(160, 102, 120, 14);
		frame.getContentPane().add(cmdAccJob2);
		
		JLabel cmdAccJob3 = new JLabel("");
		cmdAccJob3.setFont(new Font("Tahoma", Font.PLAIN, 10));
		cmdAccJob3.setBackground(Color.ORANGE);
		cmdAccJob3.setBounds(160, 127, 120, 14);
		frame.getContentPane().add(cmdAccJob3);
		
		JLabel cmdAccJob4 = new JLabel("");
		cmdAccJob4.setFont(new Font("Tahoma", Font.PLAIN, 10));
		cmdAccJob4.setBackground(Color.ORANGE);
		cmdAccJob4.setBounds(160, 152, 120, 14);
		frame.getContentPane().add(cmdAccJob4);
		
		JLabel cmdAccJob5 = new JLabel("");
		cmdAccJob5.setFont(new Font("Tahoma", Font.PLAIN, 10));
		cmdAccJob5.setBackground(Color.ORANGE);
		cmdAccJob5.setBounds(160, 177, 120, 14);
		frame.getContentPane().add(cmdAccJob5);
		
		JLabel cmdAccJob6 = new JLabel("");
		cmdAccJob6.setFont(new Font("Tahoma", Font.PLAIN, 10));
		cmdAccJob6.setBackground(Color.ORANGE);
		cmdAccJob6.setBounds(160, 202, 120, 14);
		frame.getContentPane().add(cmdAccJob6);
		
		JLabel cmdAccStatus1 = new JLabel("");
		cmdAccStatus1.setBounds(281, 77, 91, 14);
		frame.getContentPane().add(cmdAccStatus1);
		
		JLabel cmdAccStatus2 = new JLabel("");
		cmdAccStatus2.setBounds(281, 102, 91, 14);
		frame.getContentPane().add(cmdAccStatus2);
		
		JLabel cmdAccStatus3 = new JLabel("");
		cmdAccStatus3.setBounds(281, 127, 91, 14);
		frame.getContentPane().add(cmdAccStatus3);
		
		JLabel cmdAccStatus4 = new JLabel("");
		cmdAccStatus4.setBounds(281, 152, 91, 14);
		frame.getContentPane().add(cmdAccStatus4);
		
		JLabel cmdAccStatus5 = new JLabel("");
		cmdAccStatus5.setBounds(281, 177, 91, 14);
		frame.getContentPane().add(cmdAccStatus5);
		
		JLabel cmdAccStatus6 = new JLabel("");
		cmdAccStatus6.setBounds(281, 202, 91, 14);
		frame.getContentPane().add(cmdAccStatus6);
		
		JLabel cmdAccCountdown1 = new JLabel("");
		cmdAccCountdown1.setBounds(382, 77, 91, 14);
		frame.getContentPane().add(cmdAccCountdown1);
		
		JLabel cmdAccCountdown2 = new JLabel("");
		cmdAccCountdown2.setBounds(382, 102, 91, 14);
		frame.getContentPane().add(cmdAccCountdown2);
		
		JLabel cmdAccCountdown3 = new JLabel("");
		cmdAccCountdown3.setBounds(382, 127, 91, 14);
		frame.getContentPane().add(cmdAccCountdown3);
		
		JLabel cmdAccCountdown4 = new JLabel("");
		cmdAccCountdown4.setBounds(382, 152, 91, 14);
		frame.getContentPane().add(cmdAccCountdown4);
		
		JLabel cmdAccCountdown5 = new JLabel("");
		cmdAccCountdown5.setBounds(382, 177, 91, 14);
		frame.getContentPane().add(cmdAccCountdown5);
		
		JLabel cmdAccCountdown6 = new JLabel("");
		cmdAccCountdown6.setBounds(382, 202, 91, 14);
		frame.getContentPane().add(cmdAccCountdown6);
		
		cmdLineBackground = new JLabel("");
		cmdLineBackground.setBackground(Color.ORANGE);
		cmdLineBackground.setFont(new Font("Tahoma", Font.PLAIN, 16));
		cmdLineBackground.setBounds(45, 63, 750, 159);
		cmdLineBackground.setVisible(false);
		cmdLineBackground.setOpaque(true);
		frame.getContentPane().add(cmdLineBackground);
		
		JLabel cmdGlobalJobName1 = new JLabel("");
		cmdGlobalJobName1.setBounds(527, 77, 79, 14);
		frame.getContentPane().add(cmdGlobalJobName1);
		
		JLabel cmdGlobalJobName2 = new JLabel("");
		cmdGlobalJobName2.setBounds(527, 102, 79, 14);
		frame.getContentPane().add(cmdGlobalJobName2);
		
		JLabel cmdGlobalJobName3 = new JLabel("");
		cmdGlobalJobName3.setBounds(527, 127, 79, 14);
		frame.getContentPane().add(cmdGlobalJobName3);
		
		JLabel cmdGlobalJobName4 = new JLabel("");
		cmdGlobalJobName4.setBounds(527, 152, 79, 14);
		frame.getContentPane().add(cmdGlobalJobName4);
		
		JLabel cmdGlobalJobName5 = new JLabel("");
		cmdGlobalJobName5.setBounds(527, 177, 79, 14);
		frame.getContentPane().add(cmdGlobalJobName5);
		
		JLabel cmdGlobalJobName6 = new JLabel("");
		cmdGlobalJobName6.setBounds(527, 202, 79, 14);
		frame.getContentPane().add(cmdGlobalJobName6);
		
		JLabel cmdLineGlobalJobCountdown1 = new JLabel("");
		cmdLineGlobalJobCountdown1.setBounds(637, 77, 85, 14);
		frame.getContentPane().add(cmdLineGlobalJobCountdown1);
		
		JLabel cmdLineGlobalJobCountdown2 = new JLabel("");
		cmdLineGlobalJobCountdown2.setBounds(637, 102, 85, 14);
		frame.getContentPane().add(cmdLineGlobalJobCountdown2);
		
		JLabel cmdLineGlobalJobCountdown3 = new JLabel("");
		cmdLineGlobalJobCountdown3.setBounds(637, 127, 85, 14);
		frame.getContentPane().add(cmdLineGlobalJobCountdown3);
		
		JLabel cmdLineGlobalJobCountdown4 = new JLabel("");
		cmdLineGlobalJobCountdown4.setBounds(637, 152, 85, 14);
		frame.getContentPane().add(cmdLineGlobalJobCountdown4);
		
		JLabel cmdLineGlobalJobCountdown5 = new JLabel("");
		cmdLineGlobalJobCountdown5.setBounds(637, 177, 85, 14);
		frame.getContentPane().add(cmdLineGlobalJobCountdown5);
		
		JLabel cmdLineGlobalJobCountdown6 = new JLabel("");
		cmdLineGlobalJobCountdown6.setBounds(637, 202, 85, 14);
		frame.getContentPane().add(cmdLineGlobalJobCountdown6);
		
		JLabel SessionActiveCount = new JLabel("");
		SessionActiveCount.setHorizontalAlignment(SwingConstants.LEFT);
		SessionActiveCount.setBackground(Color.ORANGE);
		SessionActiveCount.setBounds(45, 0, 51, 27);
		SessionActiveCount.setVisible(false);
		frame.getContentPane().add(SessionActiveCount);
		
		JLabel SessionActiveText = new JLabel("Aktivn\u00ED: ");
		SessionActiveText.setHorizontalAlignment(SwingConstants.LEFT);
		SessionActiveText.setBackground(Color.ORANGE);
		SessionActiveText.setBounds(0, 0, 47, 27);
		SessionActiveText.setVisible(false);
		frame.getContentPane().add(SessionActiveText);
		
		JLabel systemGuardTitle = new JLabel("Secured by Sytem Guard (v1)");
		systemGuardTitle.setFont(new Font("Tahoma", Font.BOLD, 11));
		systemGuardTitle.setBackground(Color.ORANGE);
		systemGuardTitle.setBounds(109, 0, 168, 27);
		frame.getContentPane().add(systemGuardTitle);
		
		JLabel systemGuardLog = new JLabel("- System Ready");
		systemGuardLog.setForeground(Color.RED);
		systemGuardLog.setFont(new Font("Tahoma", Font.BOLD, 11));
		systemGuardLog.setBackground(Color.ORANGE);
		systemGuardLog.setBounds(281, 0, 401, 27);
		frame.getContentPane().add(systemGuardLog);
		
		
		JButton topCmdButtonConfig = new JButton("Cookies Manage");
		topCmdButtonConfig.setBackground(Color.CYAN);
		topCmdButtonConfig.setBounds(682, 0, 164, 27);
		topCmdButtonConfig.setVisible(true);
		topCmdButtonConfig.addActionListener(new ActionListener() {
		/**
		 * Po kliknut� na config button - zobrazen� configBG a v�ech tla��tek
		 */
		public void actionPerformed(ActionEvent arg0) {
			//Zobrazit cookies manage background
				configBackground.setVisible(true);
				topMenuBackground.setVisible(true);
				confDividingLine1.setVisible(true);
				confDividingLine2.setVisible(true);
				confLoadNext.setVisible(true);
				confLoadPrev.setVisible(true);
				
				confButtonInk1.setVisible(true);
				confButtonInk2.setVisible(true);
				confButtonInk3.setVisible(true);
				confButtonInk4.setVisible(true);
				confButtonInk5.setVisible(true);
				
	        try {
	        	//Z�sk�n� p�ipojen�
	        	Connection myConn = DriverManager.getConnection("jdbc:mysql://"+ConnDB_link+"/"+ConnDB_dbName, ConnDB_name, ConnDB_pass);
	        	//Vytvo�en� statement
	        	Statement myStmt = myConn.createStatement();

	            String sql = "SELECT username,ip,port,type FROM accounts WHERE assigned_session = '"+sessionID+"' LIMIT 10";
	            ResultSet rs = myStmt.executeQuery(sql);
	            //Extract data from result set
	            int poradi = 1;
	            while(rs.next()){
	            		/**
	            		 * Pro ka�d� nalezen� ��et v DB
	            		 */
	            			//Z�sk�n� jm�na ��tu z DB
	            			String confName = rs.getString("username");
	            			String igIp 	= rs.getString("ip");
	            			String igPort 	= rs.getString("port");
	            			String igType 	= rs.getString("type");
		    					//Pro 1. ��et
		            				if (poradi == 1) {
		            					//Tla��tko pro ulo�en� cookies - tla��tko s n�zvem ��tu
			            					confButtonAcc1.setText(confName);
			            					confButtonAcc1.setVisible(true);
			            					confButtonAcc1.addActionListener(new ActionListener() {
			            						public void actionPerformed(ActionEvent arg0) {
			            							//Kop�rov�n� cookies
			            								int cookiesCopy = configScripts.copyCookies(confName);
				            						//Pokud byly zkop�rov�ny �sp�n� TODO --> Dod�lat error handeling zpr�vy + p�idat break (sp� stop executoru)
			            								if (cookiesCopy == 1) {
			            									systemGuardLog.setText("Cookies �sp�n� p�id�ny pro ��et "+confName);
			            								}
			            						}
			            					});
			            				//Tla��tko pro edit / test cookies
			            					confButtonEdit1.setVisible(true);
			            					confButtonEdit1.addActionListener(new ActionListener() {
			            						public void actionPerformed(ActionEvent arg0) {
			            							//Kop�rov�n� cookies
			            								int cookiesTest = configScripts.testCookies(confName, igIp, igPort, igType);
				            						//Pokud byly zkop�rov�ny �sp�n� TODO --> Dod�lat error handeling zpr�vy + p�idat break, 
			            								if (cookiesTest == 1) {
			            									systemGuardLog.setText("Cookies �sp�n� p�id�ny pro ��et "+confName);
			            								}
			            						}
			            					});
		            				}
			    				//Pro 3. ��et
		            				if (poradi == 2) {
		            					//P�eps�n� textu tla��tka
		            					confButtonAcc2.setText(confName);
		            					confButtonAcc2.setVisible(true);
		            					confButtonAcc2.addActionListener(new ActionListener() {
		            						public void actionPerformed(ActionEvent arg0) {
		            							//Kop�rov�n� cookies
		            								int cookiesCopy = configScripts.copyCookies(confName);
		            							//Pokud byly zkop�rov�ny �sp�n�
		            								if (cookiesCopy == 1) {
		            									systemGuardLog.setText("Cookies �sp�n� p�id�ny pro ��et "+confName);
		            								}
		            						}
		            					});
		            				}
			    					//Pro 3. ��et
		            				if (poradi == 3) {
		            					//P�eps�n� textu tla��tka
		            					confButtonAcc3.setText(confName);
		            					confButtonAcc3.setVisible(true);
		            					confButtonAcc3.addActionListener(new ActionListener() {
		            						public void actionPerformed(ActionEvent arg0) {
		            							//Kop�rov�n� cookies
		            								int cookiesCopy = configScripts.copyCookies(confName);
		            							//Pokud byly zkop�rov�ny �sp�n�
		            								if (cookiesCopy == 1) {
		            									systemGuardLog.setText("Cookies �sp�n� p�id�ny pro ��et "+confName);
		            								}
		            						}
		            					});
		            				}
			    					//Pro 4. ��et
		            				if (poradi == 4) {
		            					//P�eps�n� textu tla��tka
		            					confButtonAcc4.setText(confName);
		            					confButtonAcc4.setVisible(true);
		            					confButtonAcc4.addActionListener(new ActionListener() {
		            						public void actionPerformed(ActionEvent arg0) {
		            							//Kop�rov�n� cookies
		            								int cookiesCopy = configScripts.copyCookies(confName);
		            							//Pokud byly zkop�rov�ny �sp�n�
		            								if (cookiesCopy == 1) {
		            									systemGuardLog.setText("Cookies �sp�n� p�id�ny pro ��et "+confName);
		            								}
		            						}
		            					});
		            				}
			    					//Pro 5. ��et
		            				if (poradi == 5) {
		            					//P�eps�n� textu tla��tka
		            					confButtonAcc5.setText(confName);
		            					confButtonAcc5.setVisible(true);
		            					confButtonAcc5.addActionListener(new ActionListener() {
		            						public void actionPerformed(ActionEvent arg0) {
		            							//Kop�rov�n� cookies
		            								int cookiesCopy = configScripts.copyCookies(confName);
		            							//Pokud byly zkop�rov�ny �sp�n�
		            								if (cookiesCopy == 1) {
		            									systemGuardLog.setText("Cookies �sp�n� p�id�ny pro ��et "+confName);
		            								}
		            						}
		            					});
		            				}
			    					//Pro 6. ��et
		            				if (poradi == 6) {
		            					//P�eps�n� textu tla��tka
		            					confButtonAcc6.setText(confName);
		            					confButtonAcc6.setVisible(true);
		            					confButtonAcc6.addActionListener(new ActionListener() {
		            						public void actionPerformed(ActionEvent arg0) {
		            							//Kop�rov�n� cookies
		            								int cookiesCopy = configScripts.copyCookies(confName);
		            							//Pokud byly zkop�rov�ny �sp�n�
		            								if (cookiesCopy == 1) {
		            									systemGuardLog.setText("Cookies �sp�n� p�id�ny pro ��et "+confName);
		            								}
		            						}
		            					});
		            				}
			    					//Pro 7. ��et
		            				if (poradi == 7) {
		            					//P�eps�n� textu tla��tka
		            					confButtonAcc7.setText(confName);
		            					confButtonAcc7.setVisible(true);
		            					confButtonAcc7.addActionListener(new ActionListener() {
		            						public void actionPerformed(ActionEvent arg0) {
		            							//Kop�rov�n� cookies
		            								int cookiesCopy = configScripts.copyCookies(confName);
		            							//Pokud byly zkop�rov�ny �sp�n�
		            								if (cookiesCopy == 1) {
		            									systemGuardLog.setText("Cookies �sp�n� p�id�ny pro ��et "+confName);
		            								}
		            						}
		            					});
		            				}
			    					//Pro 8. ��et
		            				if (poradi == 8) {
		            					//P�eps�n� textu tla��tka
		            					confButtonAcc8.setText(confName);
		            					confButtonAcc8.setVisible(true);
		            					confButtonAcc8.addActionListener(new ActionListener() {
		            						public void actionPerformed(ActionEvent arg0) {
		            							//Kop�rov�n� cookies
		            								int cookiesCopy = configScripts.copyCookies(confName);
		            							//Pokud byly zkop�rov�ny �sp�n�
		            								if (cookiesCopy == 1) {
		            									systemGuardLog.setText("Cookies �sp�n� p�id�ny pro ��et "+confName);
		            								}
		            						}
		            					});
		            				}
			    					//Pro 9. ��et
		            				if (poradi == 9) {
		            					//P�eps�n� textu tla��tka
		            					confButtonAcc9.setText(confName);
		            					confButtonAcc9.setVisible(true);
		            					confButtonAcc9.addActionListener(new ActionListener() {
		            						public void actionPerformed(ActionEvent arg0) {
		            							//Kop�rov�n� cookies
		            								int cookiesCopy = configScripts.copyCookies(confName);
		            							//Pokud byly zkop�rov�ny �sp�n�
		            								if (cookiesCopy == 1) {
		            									systemGuardLog.setText("Cookies �sp�n� p�id�ny pro ��et "+confName);
		            								}
		            						}
		            					});
		            				}
			    					//Pro 10. ��et
		            				if (poradi == 10) {
		            					//P�eps�n� textu tla��tka
		            					confButtonAcc10.setText(confName);
		            					confButtonAcc10.setVisible(true);
		            					confButtonAcc10.addActionListener(new ActionListener() {
		            						public void actionPerformed(ActionEvent arg0) {
		            							//Kop�rov�n� cookies
		            								int cookiesCopy = configScripts.copyCookies(confName);
		            							//Pokud byly zkop�rov�ny �sp�n�
		            								if (cookiesCopy == 1) {
		            									systemGuardLog.setText("Cookies �sp�n� p�id�ny pro ��et "+confName);
		            								}
		            						}
		            					});
		            				}
	            	poradi++;
	            }
	            rs.close();
				Thread.sleep(1000);
			} catch (InterruptedException | SQLException e) {
				e.printStackTrace();
			}

			}
		});
			        
			    

		
		frame.getContentPane().add(topCmdButtonConfig);
		frame.getContentPane().add(topMenuBackground);
		
		textStarting = new JLabel("Syst\u00E9m p\u0159ipraven ke spu\u0161t\u011Bn\u00ED");
		textStarting.setHorizontalAlignment(SwingConstants.CENTER);
		textStarting.setBackground(Color.ORANGE);
		textStarting.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textStarting.setBounds(45, 134, 750, 71);
		textStarting.setOpaque(true);
		frame.getContentPane().add(textStarting);
		
		bottomTextQuote = new JLabel(" Weeks of Programming Can Save You Hours of Planning");
		bottomTextQuote.setBounds(0, 483, 769, 14);
		frame.getContentPane().add(bottomTextQuote);
		
		bottomCopyright = new JLabel("\u00A9 2018 - \u043C\u0430\u043A\u0441\u0438\u043C\u2665  ");
		bottomCopyright.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		bottomCopyright.setHorizontalAlignment(SwingConstants.RIGHT);
		bottomCopyright.setBackground(Color.ORANGE);
		bottomCopyright.setBounds(0, 483, 846, 14);
		bottomCopyright.setOpaque(true);
		frame.getContentPane().add(bottomCopyright);
		
		lblSessionSummon = new JLabel("Session aktivn\u00ED: 10 sec");
		lblSessionSummon.setBackground(Color.ORANGE);
		lblSessionSummon.setBounds(0, 0, 846, 25);
		lblSessionSummon.setOpaque(true);
		lblSessionSummon.setVisible(false);
		
		guiBackground = new JLabel("");
		
		//Z�sk�n� re�ln� slo�ky
			String targetPath = System.getProperty("user.dir");
	    	String tagetPathFinal = targetPath+"\\img\\background.png";
		guiBackground.setIcon(new ImageIcon(tagetPathFinal));
		guiBackground.setBounds(0, 25, 846, 461);
		frame.getContentPane().add(guiBackground);

		buttonMain.addActionListener(new ActionListener() {
			/**
			 * Po kliknut� na tla��tko "Start syst�mu"
			 */
			public void actionPerformed(ActionEvent arg0) {
				//Smaz�n� tla��tka start a nahrazen� tla��tkem END
					buttonMain.setVisible(false);
					buttonEnd.setVisible(true);
					//Config
						configBackground.setVisible(false);
						confButtonAcc1.setVisible(false);
						confButtonAcc2.setVisible(false);
						confButtonAcc3.setVisible(false);
						confButtonAcc4.setVisible(false);
						confButtonAcc5.setVisible(false);
						confButtonAcc6.setVisible(false);
						confButtonAcc7.setVisible(false);
						confButtonAcc8.setVisible(false);
						confButtonAcc9.setVisible(false);
						confButtonAcc10.setVisible(false);
						
						confButtonEdit1.setVisible(false);
						confButtonEdit2.setVisible(false);
						confButtonEdit3.setVisible(false);
						confButtonEdit4.setVisible(false);
						confButtonEdit5.setVisible(false);
						confButtonEdit6.setVisible(false);
						confButtonEdit7.setVisible(false);
						confButtonEdit8.setVisible(false);
						confButtonEdit9.setVisible(false);
						confButtonEdit10.setVisible(false);
						
						confButtonInk1.setVisible(false);
						confButtonInk2.setVisible(false);
						confButtonInk3.setVisible(false);
						confButtonInk4.setVisible(false);
						confButtonInk5.setVisible(false);
						
						confDividingLine1.setVisible(false);
						confDividingLine2.setVisible(false);
						confLoadNext.setVisible(false);
						confLoadPrev.setVisible(false);
				//Zobrazen� horn� li�ty a vyps�n�
					lblSessionSummon.setVisible(true);
			/**
			 * Spu�t�n� Executoru po kliknt� na start tla��tko
			 */
				ExecutorService executorService = Executors.newSingleThreadExecutor();
					//P�ipojen� k SQL
						executorService.execute(new Runnable() {
						    public void run() {
						        try {
						        	//Zm�na textu (pozd�ji nastaven� tla��tka)
						        	buttonMain.setText("Ukon�it");
						        	//Zkou�ka DB (ToDo)
									textStarting.setText("P�ihla�ov�n� do syst�mu ...");
						        	//ToDo - tady by to mohlo ov��it jestli je syst�m ok, nen� vyplej p�es web
									Thread.sleep(3500);
									textStarting.setText("Na��t�n� pracovn�ch ��t� ...");
									Thread.sleep(3500);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
						        
						    }
						});
					//Zobrazen� TopMenu li�ty
						topMenuBackground.setVisible(true);
					//Na�ten� acc, se kter�mi bude mo�nost pracovat - pracovn� ��ty
						executorService.execute(new Runnable() {
						    public void run() {
						        try {
									cmdLineBackground.setVisible(true);
									//Zobrazit topLabel session info
										SessionActiveText.setVisible(true);
										SessionActiveCount.setVisible(true);
System.out.println("Debug [main]: Zobrazen� Session panel�, aktivace po��tadla, skryt� config element�");
									//TODO Po��tadlo bude zobrazovat jednotky (sekundy, minuty, hodiny; P��padn� posunout horn� elementy)

		            						ExecutorService Timers = Executors.newSingleThreadExecutor();
		            						Timers.execute(new Runnable() {
		            						    public void run() {
		            						        try {
		            						        	for (int i = 0; i < 60; i++) {
		            						        		SessionActiveCount.setText(Integer.toString(i));
		            						        		
		            											Thread.sleep(1000);

		            						        	}
		            								} catch (InterruptedException e) {
		            									e.printStackTrace();
		            								}
		            						        
		            						    }
		            						});	
					        	//Z�sk�n� p�ipojen�
	            						Connection myConn = DriverManager.getConnection("jdbc:mysql://"+ConnDB_link+"/"+ConnDB_dbName, ConnDB_name, ConnDB_pass);
					        	//Statement
	            						Statement myStmt = myConn.createStatement();
	            				//Samotn� SQL
	            			            String sql = "SELECT username, ip, port, type FROM 	accounts WHERE 	(active = '1' AND assigned_session = '"+sessionID+"') LIMIT 3";
	            						ResultSet rs = myStmt.executeQuery(sql);
	            				// P�i�azen� nalezen�ch jmen do listu
	            					    List<String> accListMain = new ArrayList<String>();
	            					    List<String> accListCopy = new ArrayList<String>();
	            					    List<String> accPoradiCheck = new ArrayList<String>();
	            					    ExecutorService cookiesCheckExecutor = Executors.newFixedThreadPool(10); //TODO zv��it ��slo na 20, p�idat handeling v�ce ��t� - viz ToDo list

					            //Extraktov�n� dat z result set -> Postupn� proch�zen�
						            while(rs.next()){
						            	/**
						            	 * Postupn� kontrola cookies jednotliv�ch ��t�
						            	 */
						            		/* V�pis ze SQL a ov��en� p�ihl�en� ve webdriveru*/
						            			//Z�sk�n� jm�na ��tu z DB
						            				String igName 	= rs.getString("username");
						            				String igIp		= rs.getString("ip");
						            				String igPort 	= rs.getString("port");
						            				String igType 	= rs.getString("type");
						            					// P�idat nalezen� acc do listu
						            						accListMain.add(igName);
						            					//Spustit cookies kontrolu pro nalezen� ��et
						            						cookiesCheckExecutor.execute(new Runnable() {
						            						    public void run() {
						            						        	int accountCheck = seleniumScripts.checkCookies(ConnDB_name, ConnDB_pass, ConnDB_link, ConnDB_dbName, igName, igIp, igPort, igType);
						            						        	//Pokud bylo zkontrolov�no bez chyby
						            						        		if (accountCheck == 1) {
						            							            	accPoradiCheck.add("1");
						            						        		}
										            					//Pokud byly cookies ov��eny �sp�n�, nebo nastal n�jak� error
										            						if (accountCheck == 2) {
										            							systemGuardLog.setText("Nezn�m� error p�i testov�n� cookies");
										            						}
										            						if (accountCheck == 3) {
										            							systemGuardLog.setText("Po�adovan� element (cookies test) nenalezen");
										            						}
										            						if (accountCheck == 4) {
										            							systemGuardLog.setText("Neleze p�ipojit k SQL (cookies test)");
										            						}
										            						System.out.println("Debug [main]: Za��tek kontroly ��tu "+igName);
							            						    		int poradi = accPoradiCheck.size();
													            			//Podle toho, kolik�t� v po�ad� je to account, vypsat p��slu�n� jm�no
												            					if (poradi == 1) {//Po�ad� 1
												            						//Zapsat jm�no do kolonky
												            							cmdAccName1.setText(igName);
												            						//Pokud ov��en� prob�hne �sp�n�
														            					if (accountCheck == 1) {
														            						cmdAccJob1.setText("P�ipraven");
														            					} else {
														            						//Thread.currentThread().interrupt();
														            						systemGuardLog.setText("Error p�i kontrole cookies pro ��et "+igName+"-- SystemGuard v0.1");
														            					}
												            					}//Po�ad� 1
												            					if (poradi == 2) {//Po�ad� 2
												            						//Zapsat jm�no do kolonky
												            							cmdAccName2.setText(igName);
												            						//Pokud ov��en� prob�hne �sp�n�
														            					if (accountCheck == 1) {
														            							cmdAccJob2.setText("P�ipraven");
														            					} else {
														            						systemGuardLog.setText("Error p�i kontrole cookies pro ��et "+igName+"-- SystemGuard v0.1");
														            					}
												            					}//Po�ad� 2
												            					if (poradi == 3) {//Po�ad� 3
												            						//Zapsat jm�no do kolonky
												            							cmdAccName3.setText(igName);
												            						//Pokud ov��en� prob�hne �sp�n�
														            					if (accountCheck == 1) {
														            						cmdAccJob3.setText("P�ipraven");
														            					} else {
														            						systemGuardLog.setText("Error p�i kontrole cookies pro ��et "+igName+"-- SystemGuard v0.1");
														            						executorService.shutdownNow();
														            						
														            					}
												            					}//Po�ad� 3
												            					if (poradi == 4) {//Po�ad� 4
												            						//Zapsat jm�no do kolonky
												            							cmdAccName4.setText(igName);
												            						//Pokud ov��en� prob�hne �sp�n�
														            					if (accountCheck == 1) {
														            						cmdAccJob4.setText("P�ipraven");
														            					} else {
														            						systemGuardLog.setText("Error p�i kontrole cookies pro ��et "+igName+"-- SystemGuard v0.1");
														            					}
												            					}//Po�ad� 4
												            					if (poradi == 5) {//Po�ad� 5
												            						//Zapsat jm�no do kolonky
												            							cmdAccName5.setText(igName);
												            						//Pokud ov��en� prob�hne �sp�n�
														            					if (accountCheck == 1) {
														            						cmdAccJob5.setText("P�ipraven");
														            					} else {
														            						systemGuardLog.setText("Error p�i kontrole cookies pro ��et "+igName+"-- SystemGuard v0.1");
														            					}
												            					}//Po�ad� 5
												            					if (poradi == 6) {//Po�ad� 6
												            						//Zapsat jm�no do kolonky
												            							cmdAccName2.setText(igName);
												            						//Pokud ov��en� prob�hne �sp�n�
														            					if (accountCheck == 1) {
														            						cmdAccJob6.setText("P�ipraven");
														            					} else {
														            						systemGuardLog.setText("Error p�i kontrole cookies pro ��et "+igName+"-- SystemGuard v0.1");
														            					}
												            					}//Po�ad� 6
											            						//Pokud se poda�ilo zkontrolovat cookies, zapnout timers
					System.out.println("Debug [main]: Kontrola cookies ��tu "+igName+" dokon�ena");  

						            						    }
						            						});	
						            	Thread.sleep(1000);											            											
						            } // Extraktov�n� dat z result set -> Postupn� proch�zen�
System.out.println("Debug [main]: Kontrola Cookies V�ECH ��T� dokon�ena");
						            rs.close();
						            Thread.sleep(4000);
					            	accListCopy.addAll(accListMain);
						            //Reset programu po nov�m zapnut� TODO -> dod�lat reset log�, error handeling
						            	int jobsReset = dbScripts.scriptReset(ConnDB_name, ConnDB_pass, ConnDB_link, ConnDB_dbName, accListCopy, sessionID);
						            	if (jobsReset == 1) {
System.out.println("Debug [main]: Proveden reset DB pro na�ten� ��ty");	
						            	}
						            
						            /**
						             * Cyklycky se opakuj�c�, dokud nebude ukon�eno z�sahem zvenku TODO
						             * 	-> P�idat ov��en� povolen� ke startu scriptu u� na za��tek (p�ed ov��ov�n� cookies)
						             *  -> Implementovat dal�� scripty a dod�lat handeling aktu�ln�ch
						             */
						            
						            //Defaultn� p�idat povolen� ke startu scriptu
						            	int runPerm = 1;
			/* 	Pokud bylo ud�leno povolen� pro start scriptu */
System.out.println("Debug [main]: Za��tek p�i�azov�n� nov�ch jobs");	
						            	while (runPerm == 1) {
								            /** Assign nov�ch jobs TODO -> Dod�lat v�echny errory, p�epis textu v GUI na n�zev job, otestovat*/ 
						            		//Call funkce
												accListMain.clear();
												accListMain.addAll(accListCopy);
						            			int jobsAssign = dbScripts.jobsAssign(ConnDB_name, ConnDB_pass, ConnDB_link, ConnDB_dbName, accListMain, sessionID);
						            		//Error handeling
						            			if (jobsAssign == 1) {
//System.out.println("Debug [main]: Assign nov�ch jobs prob�hl �sp�n�");	
						            			}
						            			if (jobsAssign == 2) {
						            				systemGuardLog.setText("Error p�i p�i�azov�n� nov�ch jobs-- SystemGuard v0.1");
						            				break;
						            			}
						            			
						            			
				/** Prohled�n� v�ech p�i�azen�ch prac� pro jednotliv� accounty a spu�t�n� jejich countdownu */
					            				Statement sqlState = myConn.createStatement();
						            			int accFoundMax = accPoradiCheck.size();
					            				ExecutorService jobAssignTimer = Executors.newFixedThreadPool(20); //TODO zv��it ��slo na 20, p�idat handeling v�ce ��t� - viz ToDo list
						            			String getTime = "x";
					            				String guiAssigned 	= "Nep�i�azeno";
						            			for (int accInt = 0; accInt < accFoundMax; accInt++) {
							            			String searchNick = accListCopy.get(accInt);
							            			//TODO p�idat DESC do SQL stringu?
						            				String sqlWriteJob = "SELECT job, assigned_time FROM jobs WHERE (assigned_to = '"+searchNick+"' AND session = '"+sessionID+"' AND assigned_to <> '"+sessionID+"') GROUP BY assigned_to ORDER BY assigned_time";
						            				ResultSet resultCheck = sqlState.executeQuery(sqlWriteJob);
						            				// Extraktov�n� dat z result set -> Postupn� proch�zen�
							            				while(resultCheck.next()){
								            				guiAssigned	= resultCheck.getString("job");
								            				getTime	= resultCheck.getString("assigned_time");
							            			// Z�pis do GUI
							            				// Z�skat aktu�ln� �as
												    	    Timestamp timestamp = new Timestamp(new Date().getTime());
												    	    Calendar aktualniCas = Calendar.getInstance();
												    	    aktualniCas.setTimeInMillis(timestamp.getTime());
												    	        long realTime = aktualniCas.getTimeInMillis();
												    	// Z�skat �as z DB
												    	        Calendar sqlCas = Calendar.getInstance();
												    	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
												    	        sqlCas.setTime(sdf.parse(getTime));
												    	        	long sqlTime = sqlCas.getTimeInMillis();
												    	// Z�skat rozd�ly mezi �asy v  sekund�ch
												    	        long zbyvajiciCas = TimeUnit.MILLISECONDS.toSeconds((sqlTime - realTime));
												    	// Pro 1. ��et
						            					if (accInt == 0) {
						            						cmdAccJob1.setText("Job: "+guiAssigned);;
						            						cmdAccStatus1.setText("Status: Waiting");
						            							jobAssignTimer.execute(new Runnable() {
								            						    public void run() {
								            						        try {
								            						        	for (long i = zbyvajiciCas; i > 0; i--) {
								            						        		cmdAccCountdown1.setText(Long.toString(i));
								            											Thread.sleep(1000);
								            						        	} // For
								            								} catch (InterruptedException e) {
								            									e.printStackTrace();
								            								}
								            						    } // Executor
								            						});
												    	} // Pro 1. ��et

												    	// Pro 2. ��et
						            					if (accInt == 1) {
						            						cmdAccJob2.setText("Job: "+guiAssigned);;
						            						cmdAccStatus2.setText("Status: Waiting");
						            							jobAssignTimer.execute(new Runnable() {
								            						    public void run() {
								            						        try {
								            						        	for (long i = zbyvajiciCas; i > 0; i--) {
								            						        		cmdAccCountdown2.setText(Long.toString(i));
								            											Thread.sleep(1000);
								            						        	} // For
								            								} catch (InterruptedException e) {
								            									e.printStackTrace();
								            								}
								            						    } // Executor
								            						});
												    	} // Pro 2. ��et
						            					
												    	// Pro 3. ��et
						            					if (accInt == 2) {
						            						cmdAccJob3.setText("Job: "+guiAssigned);;
						            						cmdAccStatus3.setText("Status: Waiting");
						            							jobAssignTimer.execute(new Runnable() {
								            						    public void run() {
								            						        try {
								            						        	for (long i = zbyvajiciCas; i > 0; i--) {
								            						        		cmdAccCountdown3.setText(Long.toString(i));
								            											Thread.sleep(1000);
								            						        	} // For
								            								} catch (InterruptedException e) {
								            									e.printStackTrace();
								            								}
								            						    } // Executor
								            						});
												    	} // Pro 3. ��et
						            					
												    	// Pro 4. ��et
						            					if (accInt == 3) {
						            						cmdAccJob4.setText("Job: "+guiAssigned);;
						            						cmdAccStatus4.setText("Status: Waiting");
						            							jobAssignTimer.execute(new Runnable() {
								            						    public void run() {
								            						        try {
								            						        	for (long i = zbyvajiciCas; i > 0; i--) {
								            						        		cmdAccCountdown4.setText(Long.toString(i));
								            											Thread.sleep(1000);
								            						        	} // For
								            								} catch (InterruptedException e) {
								            									e.printStackTrace();
								            								}
								            						    } // Executor
								            						});
												    	} // Pro 4. ��et
						            					
												    	// Pro 5. ��et
						            					if (accInt == 4) {
						            						cmdAccJob5.setText("Job: "+guiAssigned);;
						            						cmdAccStatus5.setText("Status: Waiting");
						            							jobAssignTimer.execute(new Runnable() {
								            						    public void run() {
								            						        try {
								            						        	for (long i = zbyvajiciCas; i > 0; i--) {
								            						        		cmdAccCountdown5.setText(Long.toString(i));
								            											Thread.sleep(1000);
								            						        	} // For
								            								} catch (InterruptedException e) {
								            									e.printStackTrace();
								            								}
								            						    } // Executor
								            						});
												    	} // Pro 5. ��et
						            					
												    	// Pro 6. ��et
						            					if (accInt == 5) {
						            						cmdAccJob6.setText("Job: "+guiAssigned);;
						            						cmdAccStatus6.setText("Status: Waiting");
						            							jobAssignTimer.execute(new Runnable() {
								            						    public void run() {
								            						        try {
								            						        	for (long i = zbyvajiciCas; i > 0; i--) {
								            						        		cmdAccCountdown6.setText(Long.toString(i));
								            											Thread.sleep(1000);
								            						        	} // For
								            								} catch (InterruptedException e) {
								            									e.printStackTrace();
								            								}
								            						    } // Executor
								            						});
												    	} // Pro 6. ��et
							            			} // Ulo�en� v�ech v�sledk� nalezen�ch v DB
						            			} // Kontrola v�ech ��t�
						            			
				/** Prohled�n� tabulky pro GLOBAL jobs, kter� je mo�no spustit */
						            			int sqlGlobalJobsResultsCount = 0;
							            			// SQL TODO - pozd�ji zv��it limit
						            					String sqlGlobalJobFind = "SELECT DISTINCT job, assigned_time FROM jobs WHERE (assigned_to = '"+sessionID+"' AND session = '"+sessionID+"') ORDER BY assigned_time LIMIT 6";
						            					ResultSet sqlGlobalJobResult = sqlState.executeQuery(sqlGlobalJobFind);
						            				// Extraktov�n� dat z result set -> Postupn� proch�zen�
							            				while(sqlGlobalJobResult.next()){
								            				String sqlGlobalJobName	= sqlGlobalJobResult.getString("job");
								            				String sqlGlobalTime	= sqlGlobalJobResult.getString("assigned_time");
								            				// Z�pis do GUI
								            				// Z�skat aktu�ln� �as
													    	    Timestamp timestamp = new Timestamp(new Date().getTime());
													    	    Calendar aktualniCas = Calendar.getInstance();
													    	    aktualniCas.setTimeInMillis(timestamp.getTime());
													    	        long realTime = aktualniCas.getTimeInMillis();
													    	// Z�skat �as z DB
													    	        Calendar sqlCas = Calendar.getInstance();
													    	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
													    	        sqlCas.setTime(sdf.parse(sqlGlobalTime));
													    	        	long sqlTime = sqlCas.getTimeInMillis();
													    	// Z�skat rozd�ly mezi �asy v  sekund�ch
													    	        long zbyvajiciCas = TimeUnit.MILLISECONDS.toSeconds((sqlTime - realTime));
													    	// Pro 1. nalezenou job
							            					if (sqlGlobalJobsResultsCount == 0) {
							            						cmdGlobalJobName1.setText("Job: "+sqlGlobalJobName);;
							            							jobAssignTimer.execute(new Runnable() {
									            						    public void run() {
									            						        try {
									            						        	for (long i = zbyvajiciCas; i > 0; i--) {
									            						        		cmdLineGlobalJobCountdown1.setText(Long.toString(i));
									            											Thread.sleep(1000);
									            						        	} // For
									            								} catch (InterruptedException e) {
									            									e.printStackTrace();
									            								}
									            						    } // Executor
									            						});
													    	} // Pro 1. nalezenou job

													    	// Pro 2. nalezenou job
							            					if (sqlGlobalJobsResultsCount == 1) {
							            						cmdGlobalJobName2.setText("Job: "+sqlGlobalJobName);;
							            							jobAssignTimer.execute(new Runnable() {
									            						    public void run() {
									            						        try {
									            						        	for (long i = zbyvajiciCas; i > 0; i--) {
									            						        		cmdLineGlobalJobCountdown2.setText(Long.toString(i));
									            											Thread.sleep(1000);
									            						        	} // For
									            								} catch (InterruptedException e) {
									            									e.printStackTrace();
									            								}
									            						    } // Executor
									            						});
													    	} // Pro 2. nalezenou job
							            					
													    	// Pro 3. nalezenou job
							            					if (sqlGlobalJobsResultsCount == 2) {
							            						cmdGlobalJobName3.setText("Job: "+sqlGlobalJobName);;
							            							jobAssignTimer.execute(new Runnable() {
									            						    public void run() {
									            						        try {
									            						        	for (long i = zbyvajiciCas; i > 0; i--) {
									            						        		cmdLineGlobalJobCountdown3.setText(Long.toString(i));
									            											Thread.sleep(1000);
									            						        	} // For
									            								} catch (InterruptedException e) {
									            									e.printStackTrace();
									            								}
									            						    } // Executor
									            						});
													    	} // Pro 3. nalezenou job
							            					
													    	// Pro 4. nalezenou job
							            					if (sqlGlobalJobsResultsCount == 3) {
							            						cmdGlobalJobName4.setText("Job: "+sqlGlobalJobName);;
							            							jobAssignTimer.execute(new Runnable() {
									            						    public void run() {
									            						        try {
									            						        	for (long i = zbyvajiciCas; i > 0; i--) {
									            						        		cmdLineGlobalJobCountdown4.setText(Long.toString(i));
									            											Thread.sleep(1000);
									            						        	} // For
									            								} catch (InterruptedException e) {
									            									e.printStackTrace();
									            								}
									            						    } // Executor
									            						});
													    	} // Pro 4. nalezenou job
							            					
													    	// Pro 5. nalezenou job
							            					if (sqlGlobalJobsResultsCount == 4) {
							            						cmdGlobalJobName5.setText("Job: "+sqlGlobalJobName);;
							            							jobAssignTimer.execute(new Runnable() {
									            						    public void run() {
									            						        try {
									            						        	for (long i = zbyvajiciCas; i > 0; i--) {
									            						        		cmdLineGlobalJobCountdown5.setText(Long.toString(i));
									            											Thread.sleep(1000);
									            						        	} // For
									            								} catch (InterruptedException e) {
									            									e.printStackTrace();
									            								}
									            						    } // Executor
									            						});
													    	} // Pro 5. nalezenou job
							            					
													    	// Pro 6. nalezenou job
							            					if (sqlGlobalJobsResultsCount == 5) {
							            						cmdGlobalJobName6.setText("Job: "+sqlGlobalJobName);;
							            							jobAssignTimer.execute(new Runnable() {
									            						    public void run() {
									            						        try {
									            						        	for (long i = zbyvajiciCas; i > 0; i--) {
									            						        		cmdLineGlobalJobCountdown6.setText(Long.toString(i));
									            											Thread.sleep(1000);
									            						        	} // For
									            								} catch (InterruptedException e) {
									            									e.printStackTrace();
									            								}
									            						    } // Executor
									            						});
													    	} // Pro 6. nalezenou job
								            				
								            				sqlGlobalJobsResultsCount++;
											            } //Zpracovat v�echny v�sledky nalezen� v DB
							            			
							            		
						            Thread.sleep(3000);
				/** Prohled�n� tabulky pro assigned jobs, kter� je mo�no spustit */
						            			// Postupn� proj�t v�echny accounty
							            			int accFound = accPoradiCheck.size() - 1;
													ExecutorService jobsExecute = Executors.newSingleThreadExecutor();
													List<String> timerEnd = new ArrayList<String>();
							            			for (int accInt = 0; accInt < accFound; accInt++) {
							            				String searchNick = accListCopy.get(accInt);
							            				timerEnd.add(searchNick);
							            				// Z�skat aktu�ln� �as
												    	    Timestamp timestamp = new Timestamp(new Date().getTime());
												    	    Calendar aktualniCas = Calendar.getInstance();
												    	    aktualniCas.setTimeInMillis(timestamp.getTime());
												    	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
												    	    String aktualineCasString  = dateFormat.format(timestamp);
							            				String sqlCheck = "SELECT job FROM jobs WHERE (assigned_to = '"+searchNick+"' AND assigned_time < '"+aktualineCasString+"' AND progress = 'assigned' AND session = '"+sessionID+"')";
							            				Statement checkStatement = myConn.createStatement();
							            				ResultSet resultCheck = checkStatement.executeQuery(sqlCheck);
							            				// Pokud je nalezena n�jak� job, kter� m� za��t
								            				while(resultCheck.next()){
									            				guiAssigned	= resultCheck.getString("job");
																Statement infoState = myConn.createStatement();
									            				String accountInfo = "SELECT username, ip, port, type FROM accounts WHERE username = '"+searchNick+"'";
									            				ResultSet resultInfo = infoState.executeQuery(accountInfo);
									            				// Extraktov�n� dat z result set -> Postupn� proch�zen�
									            				while (resultInfo.next()) {	
											            				String igIp	= resultInfo.getString("ip");
											            				String igPort	= resultInfo.getString("port");
											            				String igType	= resultInfo.getString("type");
										            					if (accInt == 0) {
										            						// Pro COMMENT Job
																				if (guiAssigned.equals("comment")) {
																					cmdAccStatus1.setText("Status: Started");
																					jobsExecute.execute(new Runnable() {
																					    public void run() {
																					    	try {
																					    		// Spustit COMMENT Job
																					    			int commentJobHandle = followScripts.commentJob(ConnDB_name, ConnDB_pass, ConnDB_link, ConnDB_dbName, searchNick, igIp, igPort, igType);
																					    		// Handeling COMMENT Job
																						    		if (commentJobHandle != 1) {
																						    			systemGuardLog.setText("Error 40"+commentJobHandle+" p�i jobComment pro ��et "+searchNick+" -- SystemGuard v0.1");
																						    		}

System.out.println("Debug [main]: Job comment pro ��et "+searchNick+" byla �sp�n� dokon�ena");	
												            						        timerEnd.remove(searchNick);
																				    	} catch (Exception e) {
																							System.out.println("Critical Error");
																						}
																					 }
																				});
	
																					// Spustit timer
																						jobAssignTimer.execute(new Runnable() {
													            						    public void run() {
													            						        try {
													            						        	for (long i = 0; i < 300; i++) {
													            						        		if (timerEnd.contains(searchNick)) {
													            						        		cmdAccCountdown1.setText(Long.toString(i));
													            						        		Thread.sleep(1000);
													            						        		} // Pokud neskon�il script
													            						        	} // For
													            								} catch (InterruptedException e) {
													            									systemGuardLog.setText("Nezn�m� error po��tadla "+searchNick+"-- SystemGuard v0.1");
													            									Thread.currentThread().interrupt();
													            								}
													            						    } // Executor
																            			});
																				} //Pro COMMENT Job
																				
											            						// Pro INK SCANNING Job
																					if (guiAssigned.equals("inkScannning")) {
																						cmdAccStatus1.setText("Status: Started");
																						jobsExecute.execute(new Runnable() {
																						    public void run() {
																						    	try {
																						    		// Spustit INK SCANNING Job
																						    			int inkScanningJobHandle = seleniumScripts.inkScanning(ConnDB_name, ConnDB_pass, ConnDB_link, ConnDB_dbName, searchNick);
																						    		// Handeling INK SCANNING Job
																							    		if (inkScanningJobHandle != 1) {
																							    			systemGuardLog.setText("Error 40"+inkScanningJobHandle+" p�i inkScanning pro ��et "+searchNick+" -- SystemGuard v0.1");
																							    		}
System.out.println("Debug [main]: Job inkScanning pro ��et "+searchNick+" byla �sp�n� dokon�ena");	
													            						        timerEnd.remove(searchNick);
																					    	} catch (Exception e) {
																								System.out.println("Critical Error");
																							}
																						 }
																					});
		
																						// Spustit timer
																							jobAssignTimer.execute(new Runnable() {
														            						    public void run() {
														            						        try {
														            						        	for (long i = 0; i < 300; i++) {
														            						        		if (timerEnd.contains(searchNick)) {
														            						        		cmdAccCountdown1.setText(Long.toString(i));
														            						        		Thread.sleep(1000);
														            						        		} // Pokud neskon�il script
														            						        	} // For
														            								} catch (InterruptedException e) {
														            									systemGuardLog.setText("Nezn�m� error po��tadla "+searchNick+"-- SystemGuard v0.1");
														            									Thread.currentThread().interrupt();
														            								}
														            						    } // Executor
																	            			});
																					} //Pro COMMENT Job
										            					}
										            					if (accInt == 1) {
										            						cmdAccJob2.setText(guiAssigned);
										            					}
										            					if (accInt == 2) {
										            						cmdAccJob3.setText(guiAssigned);
										            					}
										            					if (accInt == 3) {
										            						cmdAccJob4.setText(guiAssigned);
										            					}
										            					if (accInt == 4) {
										            						cmdAccJob5.setText(guiAssigned);
										            					}
									            				} // Pokud jsou nalezen acc informace k nalezen� job
System.out.println("Debug [main]: Pro ��et "+searchNick+" byla spu�t�na job "+guiAssigned);	
									            				
												            } // Pokud je nalezena n�jak� job, kter� m� za��t

							            			} // Postupn� proj�t v�echny accounty

						            		//Ov��en�, jestli nebylo odebr�no povolen� pro start script�
						            			runPerm = 0;
						            			Statement checkState = myConn.createStatement();
			            			            String sqlRun = "SELECT do_bot FROM shutdown WHERE session_validate = '"+sessionID+"' LIMIT 1";
			            						ResultSet resultRun = checkState.executeQuery(sqlRun);
										            //Extraktov�n� dat z result set -> Postupn� proch�zen�
											            while(resultRun.next()){
											            	runPerm++;
											            }  
											   Thread.sleep(10000);
						            	} //Pokud bylo ud�len� povolen� pro start scriptu

						           
System.out.println("Debug [main]: Syst�m byl ukon�en z�sahem z venku");

						            
						           
									Thread.sleep(1000);
								} catch (InterruptedException | SQLException e) {
									e.printStackTrace();
								} catch (ParseException e) {
									System.out.println("CHYBA V P�EVODU DATUMU");
									e.printStackTrace();
								}
						    }
						});

executorService.shutdown();
				
			}
			
		});
		
		
	}
}
