import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.mysql.jdbc.PreparedStatement;

public class followScripts {
	public static int commentJob(String ConnDB_name, String ConnDB_pass, String ConnDB_link, String ConnDB_dbName, String igName, final String igIp, final String igPort, final String igType) {
	    try {
	    	/**
	    	 * Return:
	    	 	* 1 - OK
	    	 	* 2 - Nezn�m� chyba
	    	 	* 3 - Chyba p�i hled�n� elementu (selenium)
	    	 	* 4 - Chyba SQL
	    	 	* 5 - Nezn�m� chyba funkce
	    	 */
	    	
	  /** Update progressu SQL */
	    	// Nav�z�n� p�ipojen�
    			Connection myConn = DriverManager.getConnection("jdbc:mysql://"+ConnDB_link+"/"+ConnDB_dbName, ConnDB_name, ConnDB_pass);
		    // Z�sk�n� aktu�ln�ho �asu
    			Timestamp timestamp = new Timestamp(new Date().getTime());
			    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			    String aktualniCas  = dateFormat.format(timestamp);
		    		
System.out.println("Debug [followScripts]: Byla zapnuta job comment pro ��et "+igName);
	            // Zm�na progressu a nastaven� �asu v SQL
					try {
					  // Vytov�en� statementu
						PreparedStatement ps = (PreparedStatement) myConn.prepareStatement(
							"UPDATE jobs SET progress = ?, assigned_time = ? WHERE (job = ? AND assigned_to = ?)");
					
					  // Nastaven� parametr�
						  ps.setString(1,"started");
						  ps.setString(2,aktualniCas);
						  ps.setString(3,"comment");
						  ps.setString(4,igName);
					
					  // Zavol�n� funkce updatu a ukon�en� p�ipojen�
						  ps.executeUpdate();
						  ps.close();
					}
					catch (SQLException se) {
					  return 4;
					}
	    	
	/** Selenium */
		     ChromeOptions chromeOptions = new ChromeOptions();
		     //Z�skat aktu�ln� slo�ku
		      	String systemPath = System.getProperty("user.dir");
		     //Nastaven� Chrome options
			      chromeOptions.addArguments("user-data-dir="+systemPath+"//chrome_profiles//"+igName);
			      chromeOptions.setBinary("C:/Users/Maxim/AppData/Local/Google/Chrome SxS/Application/chrome.exe");
			 //Nastaven� proxy IP
			      //Pokud je type socks
			      	if (igType.equals("socks")) {
			      		chromeOptions.addArguments("--proxy-server=socks5://" + igIp + ":" + igPort);
System.out.println("Debug [seleniumScripts]: Proxy nastavena na typ Socks5, na��t�n� ip: "+igIp+" s portem "+igPort);
			      	}
		    //Start Chrome
			      	WebDriver Driver = new ChromeDriver(chromeOptions);
System.out.println("Debug [seleniumScripts]: Otev�r�n� cookies profilu "+systemPath+"//chrome_profiles//"+igName); 
	/** Samotn� script */
		      Driver.navigate().to("http://mojeip.cz");
		      Thread.sleep(4000);
		      Driver.quit();
		      /*
		      try {
			      int login = Driver.findElements(By.xpath("//button[@class='_q8y0e coreSpriteMobileNavSettings _8scx2']")).size();
			      if (login == 0) {
			    	  Driver.quit();
			    	  return 3;
			      } else {
			    	  Driver.quit();
			    	  return 1;
			      }
			  
		      } catch (Exception exc) {
			    	Driver.quit();
			    	return 3;
		      }
		    */
		    		
System.out.println("Debug [followScripts]: Byla dokon�ena job comment pro ��et "+igName+". N�slednuje smaz�n� z SQL");
	            // Smaz�n� z�znamu z SQL
					try {
						  // Vytov�en� statementu
							PreparedStatement deleteState = (PreparedStatement) myConn.prepareStatement(
								"DELETE FROM jobs WHERE (job = ? AND assigned_to = ?)");
						
						  // Nastaven� parametr�
							deleteState.setString(1,"comment");
							deleteState.setString(2,igName);
						
						  // Zavol�n� funkce updatu a ukon�en� p�ipojen�
							deleteState.executeUpdate();
							deleteState.close();
							return 1;
						} // Try - delete
						catch (SQLException se) {
						  return 4;
						}    
	    } // Try cel� funkce
	    catch (Exception exc) {
	      exc.printStackTrace();
	      	return 5;
	    }
	    
	}
}
